# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-buildx
pkgver=0.20.0
pkgrel=0
_commit=8e30c4669ca5aace9dd682650053c307f75fe5cc
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/engine/reference/commandline/buildx_build"
arch="all"
license="Apache-2.0"
makedepends="go"
options="net"
source="buildx-$pkgver.tar.gz::https://github.com/docker/buildx/archive/v$pkgver.tar.gz"

_buildx_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/buildx-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/buildx
	local ldflags="-X $PKG/version.Version=v$pkgver -X $PKG/version.Revision=$_commit -X $PKG/version.Package=$PKG"
	go build -v -modcacherw -ldflags "$ldflags" -o docker-buildx ./cmd/buildx
}

check() {
	# bake and gitutil tests do not succeed inside abuild environment
	local pkgs="$(go list -modcacherw ./... | grep -Ev '(bake|gitutil)')"
	go test -modcacherw -short $pkgs
	./docker-buildx version
}

package() {
	# this is circular to have top-level, so depend on it in package itself
	depends="docker-cli"
	install -Dm755 docker-buildx "$pkgdir$_buildx_installdir"/docker-buildx
}

sha512sums="
9234446325f38498e46ef0314515d9f0df9477c1f3a3316cdcd1f66353145bba986cc3cf20317b722b5f0d26ca2c57fdf4a730d6ae9286c0b39e00d0d8aaa628  buildx-0.20.0.tar.gz
"
